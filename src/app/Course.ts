export class Course {
    constructor(
        private id:number, 
        private name:string, 
        private price:number, 
        private rating?:number,
        private students?:number,
        private courseUrl?:string,
        private desc?:string       
    ){}
}