import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Course } from '../Course';
import { CoursesService } from '../services/courses.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  constructor(private courseService:CoursesService, private router:Router) { }

  ngOnInit(): void {
  }

  courseForm = new FormGroup({
    coursename: new FormControl('',
    [
      Validators.required, 
      Validators.minLength(5), 
      Validators.maxLength(10)
    ]),
    price: new FormControl('',
    [
      Validators.required
    ]),
    offer: new FormControl('true')
  });

  get coursename(){
    return this.courseForm.get("coursename")
  }

  get price(){
    return this.courseForm.get("price")
  }

  get offer(){
    return this.courseForm.get("offer")
  }

  submitForm(){
    const {coursename: name, price, offer} = this.courseForm.value;
    let course:Course = new Course(Math.random(), name, price, 5);
    this.courseService.submitCourse(course).subscribe(course => console.log ('created')) ;
    this.router.navigate(['/courses']);
  }

}
