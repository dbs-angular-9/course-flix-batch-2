import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-custom-directive',
  templateUrl: './custom-directive.component.html',
  styleUrls: ['./custom-directive.component.css']
})
export class CustomDirectiveComponent implements OnInit, AfterViewInit {

  constructor() { }
  @ViewChild('input') value: ElementRef;

  phone:string;

  ngOnInit(): void {
    console.log('Inside the ng On Init', this.value);
  }


  ngAfterViewInit(){
    console.log('Inside the after view init ', this.value);
  }

  

  






}
