import { Component, OnInit, OnDestroy } from '@angular/core';
import { Course } from '../Course';
import { CoursesService } from '../services/courses.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-t-form',
  templateUrl: './t-form.component.html',
  styleUrls: ['./t-form.component.css']
})
export class TFormComponent implements OnInit , OnDestroy{

  constructor( private courseService: CoursesService, private router:Router) { }

  ngOnInit(): void {
    console.log("Inside the on init method")
  }

  ngOnDestroy(){
    console.log("Caleld the destroy method on TForm");
  }

  handleChange(obj) {
    console.log(obj);
  }
  
  submitCourse({coursename:name, price}){
    console.log("came inside the submit course function");
    let course = {
      courseUrl: '', 
      name, 
      price, 
      rating:5, 
      students: 0, 
      desc:"new course"
    };

    this.courseService.submitCourse(course).subscribe(data => console.log(data));
    this.router.navigate(["/courses"]);
  }

}
