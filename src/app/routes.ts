import { CoursesComponent } from './courses/courses.component';
import { FormComponent } from './form/form.component';
import { TFormComponent } from './t-form/t-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from './auth.guard';
import { ConfirmGuard } from './confirm.guard';
import { LoginComponent } from './login/login.component';
import { CustomDirectiveComponent } from './custom-directive/custom-directive.component';
import { Route } from '@angular/compiler/src/core';
import { Routes } from '@angular/router';

export const routes:Routes= [
    {
      path:'',
      redirectTo:'courses',
      pathMatch:'full'
    },
    {
      path:'directive',
      component:CustomDirectiveComponent
    },
    {
      path:'login',
      component:LoginComponent
    },
    {
      path:'courses',
      component: CoursesComponent
    },
    {
      path:'courses/add',
      component:FormComponent,
      canDeactivate:[ConfirmGuard],
    
      children:[
        {
          path:'',
          redirectTo:"tform",
          pathMatch:'full'
          
        },
        {
          path:'tform',
          component:TFormComponent
        },
        {
          path:'rform',
          component:ReactiveFormComponent
        }
      ]
    },
    {
      path:'courses/:id',
      component:CourseDetailsComponent
    },
    {
      path:'orders',
      loadChildren:()=> import('./orders/orders.module').then(m => m.OrdersModule)
    },
    {
      path:'**',
      component:NotFoundComponent
    }
  ]