import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Course } from '../Course';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent {

  @Input('data') course;

  @Output("select") update = new EventEmitter();


  select(course){
    this.update.emit(course);
  }

}
