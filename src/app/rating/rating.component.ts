import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  constructor() { }

  stars:number[]=[];

  halfStar = false;
  
  @Input("rating") rating;

  ngOnInit(): void {
    let whole =  Math.floor(this.rating);
    this.stars = new Array(whole);
    if(this.rating - whole !== 0 ){
      this.halfStar = true;
    }
  }
}
