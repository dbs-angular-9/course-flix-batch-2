import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { FormComponent } from './form/form.component';

@Injectable({
  providedIn: 'root'
})
export class ConfirmGuard implements CanDeactivate<FormComponent> {
  canDeactivate(component: FormComponent, 
                currentRoute: ActivatedRouteSnapshot, 
                currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot):boolean{
   return confirm("Do you really want to exit without saving ?");
  }
}
