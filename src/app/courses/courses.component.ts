import { Component, OnInit } from '@angular/core';
import { Course } from '../Course';
import { CoursesService } from '../services/courses.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  title = 'Welcome to React Workshop day 2 !!';
   //course="react";
   assetUrl = "../assets/react.png";
   theme="deepawali";
   flag = false;
   courses:any[] = [];
   subscription: Subscription;
   
   //1. Have the service injected - constructor
   constructor(private courseService:CoursesService){}

   //2. Load the courses on component initialization - onInit
   
   ngOnInit(){
     
     /* const promiseData = this.courseService.fetchAllCoursesUsingPromise();
      const dataPromise = promiseData.then(res => res.json());
      dataPromise.then(data => {
        console.log("Inside the fetch method "+ data)
        this.courses = data
      });*/

      this.subscription = this.courseService.fetchAllCourses()
          .subscribe(courses => {
            this.courses = courses
          });
   }

   ngOnDestroy(){
     this.subscription.unsubscribe();
   }

   
 
   handleClick(){
     console.log('came inside the handle click function ... ')
   }
 
   description = 'Lorem ipsum dolor sit amet consectetur ...';
 
 
   //lab - 2
   select(course){
    this.courses.forEach(c => {
      if ( c.id === course.id){
        c.students++;
      }
    })
   }
 

}
