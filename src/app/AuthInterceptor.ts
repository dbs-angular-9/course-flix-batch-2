import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private router:Router){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        
        const token = localStorage.getItem("userToken");
        if (!token) {
            this.router.navigate(['/login']);
        } else {
            req = req.clone({
                     setHeaders: {'Authorization': `Bearer ${token}`}
                 })
        }
        return next.handle(req);
    }
    
}