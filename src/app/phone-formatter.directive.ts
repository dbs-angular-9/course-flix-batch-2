import { Directive, Input, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[format]'
})
export class PhoneFormatterDirective {


  @Input("format") countryCode;

  constructor(private elem: ElementRef) { }

  @HostListener("blur")
  onBlur(){
    console.log("Cam inside the blur metthod");
    console.log(this.elem.nativeElement.value);
    console.log(`The coutty code is ${this.countryCode}`)
    this.formatInput();
  }

  formatInput(){
    if(this.countryCode == 'IND'){
      this.elem.nativeElement.value = `+91 - ${this.elem.nativeElement.value}`
    }
  }



}
