import { Injectable } from '@angular/core';
import { Course } from '../Course';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {retry, catchError, timeout, filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

 
  constructor(private httpClient:HttpClient) { }

  API_URL="https://my-json-server.typicode.com/prashdeep/courseflix/courses";

  fetchAllCourses():Observable<Course[]>{
    return this.httpClient
        .get<Course[]>(this.API_URL);
  }
  

  fetchAllCoursesUsingPromise(){
    const d = fetch(this.API_URL)
    console.log('promise '+d);
    return fetch(this.API_URL);
  }

  
  fetchCourseDetails(id:number):Observable<Course>{
    return this.httpClient
          .get<Course>(`${this.API_URL}/${id}`)
          .pipe(
            retry(2),
            catchError(this.handleError)
          )
  }

  submitCourse(course):Observable<Course>{
    return this.httpClient.post<Course>(`${this.API_URL}`, course);
  }

  deleteCourse(id:number):Observable<void>{
    return this.httpClient.delete<void>(`${this.API_URL}/${id}`);
  }

  handleError(error:HttpErrorResponse){
    console.log(error);
    return throwError(error);

  }
}
