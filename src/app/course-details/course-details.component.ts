import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../services/courses.service';
import { Course } from '../Course';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {


  course;

  constructor(
    private courseService:CoursesService,
    private activatedRoute:ActivatedRoute
    ) { }

  ngOnInit(): void {
    const courseId = this.activatedRoute.snapshot.paramMap.get("id");
    this.courseService.fetchCourseDetails(+courseId)
            .subscribe(course => this.course = course);
  }

}
