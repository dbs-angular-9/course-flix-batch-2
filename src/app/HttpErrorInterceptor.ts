import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

export class HttpErrorInterceptor implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        return next.handle(req)
                .pipe(
                    retry(5),
                    catchError((errorResponse:HttpErrorResponse) => {
                        if (errorResponse.error instanceof ErrorEvent) {
                            console.log("Client side error");
                        } else {
                            console.log("server side error");
                        }
                        return throwError(errorResponse.message);
                    })
                )
    }
}