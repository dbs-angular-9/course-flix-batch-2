import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trim'
})
export class TrimPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    console.log(`value : ${value} and args[0] ${args[0]}`);

    return value.replace(' ', args[0]);
  }

}
