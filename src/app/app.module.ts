import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { RatingComponent } from './rating/rating.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {RouterModule} from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { TFormComponent } from './t-form/t-form.component';
import {FormsModule,  ReactiveFormsModule} from '@angular/forms';
import { FormComponent } from './form/form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { routes } from './routes';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpErrorResponse} from '@angular/common/http';
import { TrimPipe } from './trim.pipe';
import { HttpErrorInterceptor } from './HttpErrorInterceptor';
import { LoginComponent } from './login/login.component';
import { AuthInterceptor } from './AuthInterceptor';
import { PhoneFormatterDirective } from './phone-formatter.directive';
import { CustomDirectiveComponent } from './custom-directive/custom-directive.component';
import { OrdersModule } from './orders/orders.module';

@NgModule({
  declarations: [
    AppComponent,
    CourseComponent,
    RatingComponent,
    CoursesComponent,
    CourseDetailsComponent,
    NavBarComponent,
    NotFoundComponent,
    TFormComponent,
    FormComponent,
    ReactiveFormComponent,
    TrimPipe,
    LoginComponent,
    PhoneFormatterDirective,
    CustomDirectiveComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide:HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
